package com.rigar.roboto;

import java.awt.AWTException;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.Scanner;

public class Roboto {
	public static void main(String args[]) {
		try {
			@SuppressWarnings("resource")
			Scanner in = new Scanner(System.in);
			
			System.out.println("Ingrese frase a escribir");
			String cadenaIngresada = in.nextLine();
			
			System.out.println("Ingrese el numero de repeticiones");
			Integer numeroRepeticiones = in.nextInt();
			
			System.out.println("Ingrese el intervalo de repetición en milisegundos");
			Integer intervalo = in.nextInt();
			
			System.out.println("Comenzara en 5 segundos");
			
			Robot robot = new Robot();
			robot.delay(5000);

			for (int i = 0; i < numeroRepeticiones; i++) {
				robot.delay(intervalo);
				for (int j = 0; j < cadenaIngresada.length(); j++) {
					robot.keyPress(KeyEvent.getExtendedKeyCodeForChar(cadenaIngresada.charAt(j)));
				}
				robot.keyPress(KeyEvent.VK_ENTER);
			}

		} catch (AWTException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
	}
}
